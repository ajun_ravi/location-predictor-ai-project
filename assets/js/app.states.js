var app = angular.module('AI', ["ngRoute"]);
app.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "app/views/canvas.htm",
            controller:"canvas",
            controllerAs:"can"
        }).
        otherwise({
            templateUrl: "app/views/404.html"
        });
});
