var express = require('express');
var fs = require('fs');
var app = express();
var jsonexport = require('jsonexport');
app.use(express.static(__dirname + '/'));
app.get('/',function(req,res){
    res.sendFile(__dirname + '/index.htm');
});
app.get('/convert',function(req,res){
   
    var reader = fs.createReadStream('./assets/json/windermere.json');
    var writer = fs.createWriteStream('./assets/csv/windermere.csv');
    reader.pipe(jsonexport()).pipe(writer);
    res.send('Done');
    
});
app.listen(2323,function(server){});