var app = angular.module('AI');
app.run(function ($rootScope, $location) {
    $rootScope.start = 0;
});
app.controller('canvas',function($scope){
    var std = 0;
    var t = '-=2px';
    var l = '-=2px';
    var a = -111; var aa = -222;
    var step = []; var flag = 0,cyc = 0,diff = -1,act = '+',from= -111,from2 = -111,tch = 0,start=0;
    var brain = [];
    var neuronA1 = 0;//379;
    var neuronA2 = 0;//424;
    var neuronB1 = 0;//898;
    var neuronB2 = 0;//942;
    setInterval(function(){
        var bt = $('#ball').css('margin-top').substring(0, ($('#ball').css('margin-top').length - 2));
        var bl = $('#ball').css('margin-left').substring(0, ($('#ball').css('margin-left').length - 2));
        
        if(bt >= 235){
            t = '-=2px';
            while (step.length > 0) {
                step.pop();
            }
            if(start == 1)
                $('body').css('background','red');
            // $('#logic').val($('#logic').val() + ' ' + bl);
        }
        else if(bt <= 2){
            flag = 0; t = '+=2px';
            if (diff != -1)
            tch = 1;
        }
            
        if (bl >= 478){
            flag = 0; l = '-=2px';
        }
        else if (bl <= 0){
            flag = 0; l = '+=2px';
        }
            
        $('#ball').css('margin-left', l);
        $('#ball').css('margin-top', t);
    
        var hl = $('#hand').css('margin-left').substring(0, ($('#hand').css('margin-left').length - 2));
        var hoff = $('#hand').offset().left; 
        var boff = $('#ball').offset().left; 
        if (brain.length > 0 && diff != -1 && tch == 1) {
            // $('#logic').val($('#logic').val() + ' ---  ' + brain[0] + ',' + brain[1] + ',' + brain[2] + ',' + brain[3]);
            tch = 0; 
            if (brain[3] > brain[1]) {
                var b = brain[1] + diff;
            }
            else if (brain[3] < brain[1]) {
                var b = brain[1] - diff;
            }
            if (brain[2] > brain[0]) {
                 a = brain[2] + diff;
            }
            else if (brain[2] < brain[0]) {
                 a = brain[2] - diff;
            }

            
            if (a < neuronA1)
                a = (2 * neuronA2) - a - (neuronA2 - neuronA1);
            if (a >= neuronA1 && a < neuronA2)
                a = (2 * neuronA2) - a;
            
            if (a > neuronB2)
                a = (2 * neuronB2) - a - (neuronB2 - neuronB1);
            if (a >= neuronB1 && a < neuronB2)
                a = (2 * neuronB1) - a;
            $('#logic').val($('#logic').val() + ' , ' + a);
            // $('#hand').offset({ left: a });
            brain[0] = brain[1];
            brain[1] = brain[2];
            brain[2] = brain[3];
            brain[3] = a;
        }
        if (bt >= 219 && boff >= (hoff - 40) && boff <= (hoff + 40)){ 
            t = '-=2px'; 
            if (start == 1) {
                stop = 0;
            }
             if(flag == 0){
                 
                 flag = 1;
                 if (cyc == 0) {
                     step.push(boff);
                     if (step.length == 4) {
                         cyc = 1;
                         diff = step[0] - step[2];
                         if(diff < 0) diff = diff * -1;
                         diff = diff + 8;
                         stop = 0; 
                         brain = step;                         
                         $('.controls').remove();
                         $('#state').text('Ok learned. System is playing now..');
                     }
                 }
             }
            //     $('#logic').val($('#logic').val() + ' , bl:' + bl + ' hl:' + hl );
         }

        var pos = $('#hand').offset().left;
        if ((pos - 10) <= a && (pos + 10) >= a && a != -111){
            if(aa != a){
                stop = 1; start = 1;
                aa = a;
            }
        }
},50);
     
    var h = '-=2px'; var stop = 0;
    var st = 0;
    $scope.left = function(){
        if (stop == 0)
            stop = 1;
        else
            stop = 0;
         $('#hand').css('margin-left', '-=5px');
         h = '-=2px';
         $st = 1;
     };
     $scope.right = function () {
         if(stop == 0)
         stop = 1;
         else
         stop = 0;
         $('#hand').css('margin-left', '+=5px');
         h = '+=2px';
         $st = 1;
     };

     setInterval(function(){
        if(stop == 0){
            var hl = $('#hand').css('margin-left').substring(0, ($('#hand').css('margin-left').length - 2));
            if(start == 1){
                var po = $('#hand').offset().left;
                if (a > po) h = '+=2px';
                else h = '-=2px';
            }
            else{
                if (hl <= -471){
                    h = '+=2px';
                    neuronA2 = $('#hand').offset().left;
                    neuronA1 = neuronA2 - 45;
                }
                else if (hl >= 474){
                    h = '-=2px';
                    neuronB1 = $('#hand').offset().left;
                    neuronB2 = neuronB1 + 44;
                }
            }
            $('#hand').css('margin-left', h);
        }
        
        if (neuronA1 != 0 && neuronA2 != 0 && neuronB1 != 0 && neuronB2 != 0){
            $('.controls').show();
            if($('#state').text() != 'Ok learned. System is playing now..')
            $('#state').text('Listening your play..');
            console.log(neuronA1 + ' ' + neuronA2 + ' and ' +  neuronB1 + ' ' +  neuronB2);
        }
              
        
     },10);



});

